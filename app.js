require('dotenv').config()
const express = require('express');
const path = require('path');
const cors = require('cors');

const blurt = require('@blurtfoundation/blurtjs');
const _ = require('lodash');
const moment = require('moment');
const mongoose = require('mongoose');

const RPC_URL = process.env.RPC_URL || 'https://rpc.blurt.world';
const BLOCK_GET_INTERVAL = parseFloat(process.env.BLOCK_GET_INTERVAL || 2500);
const GLOBAL_GET_INTERVAL = parseFloat(process.env.GLOBAL_GET_INTERVAL || 3000);

blurt.api.setOptions({ url: RPC_URL, useAppbaseApi: true });

const api = require('./src/api');
const app = express();

var db1 = mongoose.createConnection(process.env.MONGODB_URI, { useNewUrlParser: true });
const { AccountSchema } = require('./src/schemas');
const Account = db1.model('Account', AccountSchema);
const models = { Account };

db1.then(async (err, result) => {
  streamChain();
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

var corsOptions = {
  origin: function (origin, callback) {
    callback(null, true)
  },
  credentials: true,
}

app.use(express.json());
app.use(cors(corsOptions));
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api', api({}, models));

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

// --------------- FUNCTIONS ----------------- //

//This method will start on bot ready...
function streamChain() {
  let head_block = 0;
  let last_block = 0;

  setInterval(function () {
    blurt.api.getDynamicGlobalProperties(function (err, result) {
      if (!_.isEmpty(result)) head_block = result.last_irreversible_block_num;
    });
  }, GLOBAL_GET_INTERVAL);

  setInterval(function () {
    if (head_block > last_block) {
      if (last_block === 0) {
        last_block = head_block;
      }
      processBlock(last_block)
      last_block = last_block + 1;
    }
  }, BLOCK_GET_INTERVAL);

} // Send activity function end.

function processBlock(current_block) {
  blurt.api.getOpsInBlock(current_block, false, (err, result) => {
    if (err) {
      console.log('get block error', err);
      return;
    }

    // process un-empty blocks only...
    if (!_.isEmpty(result)) {
      // update account for any transactions that increases an account's value
      let process_txs = [
        'author_reward', 'comment_benefactor_reward', 'comment_reward', 'curation_reward',
        'fill_convert_request', 'fill_order', 'fill_transfer_from_savings', 'fill_vesting_withdraw',
        'liquidity_reward', 'producer_reward', 'proposal_pay', 'return_vesting_delegation',
        'sps_fund', 'withdraw_vesting', 'claim_reward_balance',
      ];
      result.forEach(tx => {
        let txType = tx.op[0];
        let txData = tx.op[1];
        if (process_txs.indexOf(txType) >= 0) {
          if (txType === "withdraw_vesting" || txType === "claim_reward_balance") {
            console.log('block_number', current_block)
            console.log('txType', txType)
            updateAccountInfo(txData.account)

          } else if (txType === "fill_vesting_withdraw") {
            console.log('block_number', current_block)
            console.log('txType', txType)
            updateAccountInfo(txData.from_account)
            updateAccountInfo(txData.to_account)

          } else if (txType === "transfer_to_vesting") {
            console.log('block_number', current_block)
            console.log('txType', txType)
            updateAccountInfo(txData.from)
            updateAccountInfo(txData.to)
          }

        }
      });
    }
  });
}

function updateAccountInfo(account) {
  console.log('updating account info', account)
  blurt.api.getAccounts([account], async function (err, result) {
    if (err) return;
    let account_info = result[0];

    let {
      balance, vesting_shares, vesting_withdraw_rate, next_vesting_withdrawal,
      delegated_vesting_shares, received_vesting_shares, savings_balance,
      last_account_recovery, last_account_update, last_owner_update,
      last_post, last_root_post, last_vote_time,
    } = account_info;
    let activity_dates = [
      last_account_recovery, last_account_update, last_owner_update,
      last_post, last_root_post, last_vote_time,
    ];
    let last_activity_date = new Date(Math.max(...activity_dates.map(x => new Date(x + "Z"))));

    Account.findOne({ name: account }).then(result => {
      if (!result) {
        console.log('no account record ', result)
        console.log('inserting new account...', account)
        result = new Account();
      }

      balance = balance.replace(' BLURT', '');
      vesting_shares = vesting_shares.replace(' VESTS', '');
      savings_balance = savings_balance.replace(' BLURT', '');
      total_value = (parseFloat(balance) + parseFloat(vesting_shares) + parseFloat(savings_balance)).toFixed(3);

      _.extend(result, {
        name: account,
        balance,
        vesting_shares,
        vesting_withdraw_rate: vesting_withdraw_rate.replace(' VESTS', ''),
        next_vesting_withdrawal: new Date (next_vesting_withdrawal + "Z"),
        delegated_vesting_shares: delegated_vesting_shares.replace(' VESTS', ''),
        received_vesting_shares: received_vesting_shares.replace(' VESTS', ''),
        savings_balance,
        total_value,
        last_active_date: last_activity_date,
      });
      result.save().catch(error => {
        if (error.name === 'MongoError' && error.code === 11000) {
          console.log('duplicate error: ', account)
        }
      });
    });
  });
}