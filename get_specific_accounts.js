require('dotenv').config();
const accounts = require('./accounts.json');

const blurt = require('@blurtfoundation/blurtjs');
const _ = require('lodash');
const mongoose = require('mongoose');

const RPC_URL = process.env.RPC_URL || 'https://rpc.blurt.world';
blurt.api.setOptions({ url: RPC_URL, useAppbaseApi: true });

var db1 = mongoose.createConnection(process.env.MONGODB_URI, { useNewUrlParser: true });
const { AccountSchema } = require('./src/schemas');
const Account = db1.model('Account', AccountSchema);

db1.then(async (err, result) => {
  console.log('accounts', accounts.names);
  get_accounts(accounts.names);
});

// --------------- FUNCTIONS ----------------- //
function updateAccountInfo(account) {
  blurt.api.getAccounts([account], async function (err, result) {
    if (err) return;
    let account_info = result[0];
    console.log('account_info', account_info)

    let {
      balance, vesting_shares, vesting_withdraw_rate, next_vesting_withdrawal,
      delegated_vesting_shares, received_vesting_shares, savings_balance,
      last_account_recovery, last_account_update, last_owner_update,
      last_post, last_root_post, last_vote_time,
    } = account_info;
    let activity_dates = [
      last_account_recovery, last_account_update, last_owner_update,
      last_post, last_root_post, last_vote_time,
    ];
    let last_activity_date = new Date(Math.max(...activity_dates.map(x => new Date(x + "Z"))));

    Account.findOne({ name: account }).then(result => {
      if (!result) {
        console.log('no account record ', result)
        console.log('inserting new account...', account)
        result = new Account();
      }

      balance = balance.replace(' BLURT', '');
      vesting_shares = vesting_shares.replace(' VESTS', '');
      savings_balance = savings_balance.replace(' BLURT', '');
      total_value = (parseFloat(balance) + parseFloat(vesting_shares) + parseFloat(savings_balance)).toFixed(3);

      _.extend(result, {
        name: account,
        balance,
        vesting_shares,
        vesting_withdraw_rate: vesting_withdraw_rate.replace(' VESTS', ''),
        next_vesting_withdrawal: new Date (next_vesting_withdrawal + "Z"),
        delegated_vesting_shares: delegated_vesting_shares.replace(' VESTS', ''),
        received_vesting_shares: received_vesting_shares.replace(' VESTS', ''),
        savings_balance,
        total_value,
        last_active_date: last_activity_date,
      });
      result.save().catch(error => {
        if (error.name === 'MongoError' && error.code === 11000) {
          console.log('duplicate error: ', account)
        }
      });
    });
  });
}

async function get_accounts(accounts) {
  for (let i = 0; i < accounts.length; i++) {
    let account = accounts[i];
    updateAccountInfo(account);
  }
}