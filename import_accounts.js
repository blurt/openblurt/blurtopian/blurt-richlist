require('dotenv').config()
const blurt = require('@blurtfoundation/blurtjs');
const _ = require('lodash');
const moment = require('moment');
const mongoose = require('mongoose');
const CronJob = require('cron').CronJob;

const RPC_URL = process.env.RPC_URL || 'https://rpc.blurt.world';
const EVERY_MINUTE = "0 * * * * *";
const EVERY_HOUR = "0 0 * * * *";

blurt.api.setOptions({ url: RPC_URL, useAppbaseApi: true });

var db1 = mongoose.createConnection(process.env.MONGODB_URI, { useNewUrlParser: true });
const { AccountSchema } = require('./src/schemas');
const Account = db1.model('Account', AccountSchema);

const job = new CronJob(EVERY_MINUTE, function() {
});
job.start();
db1.then(async (err, result) => {
  get_all_accounts();
});

async function get_all_accounts(start='', stop='', steps=1e3, limit=-1) {
  let cnt = 1;
  let lastname = ""
  if (start !== "") {
    lastname = start
  }

  try {
    while (true) {
      await new Promise(r => setTimeout(r, 500));
      let ret = await blurt.api.lookupAccountsAsync(lastname, steps);
      console.log('ret', ret)

      let account_name = "";
      for (let i = 0; i < ret.length; i++) {
        let account = ret[i];
        try {
          let account_info = await blurt.api.getAccountsAsync([account]);
          console.log('account_info', account_info)
          let item = new Account();
          _.extend(item, {
            name: account,
            ...account_info[0]
          });
          await item.save();
        } catch (err) {
          console.log('mongodb err', err)
        }

        account_name = account;
        if (account_name != lastname) {
          cnt += 1
          if (account_name == stop || (limit > 0 && cnt > limit)) {
            return
          }
        }
      }
      console.log('lastname == account_name', lastname == account_name)
      if (lastname == account_name) {
        return
      }

      lastname = account_name
      console.log('ret.length < steps', ret.length < steps)
      if (ret.length < steps) {
        return
      }
    }
  } catch (err) {
    console.log('err', err)
  }

}