require('dotenv').config()
const blurt = require('@blurtfoundation/blurtjs');
const _ = require('lodash');
const moment = require('moment');
const mongoose = require('mongoose');
const CronJob = require('cron').CronJob;

const RPC_URL = process.env.RPC_URL || 'https://rpc.blurt.world';
const EVERY_MINUTE = "0 * * * * *";
const EVERY_HOUR = "0 0 * * * *";

blurt.api.setOptions({ url: RPC_URL, useAppbaseApi: true });

var db1 = mongoose.createConnection(process.env.MONGODB_URI, { useNewUrlParser: true });
const { AccountSchema } = require('./src/schemas');
const Account = db1.model('Account', AccountSchema);

db1.then(async (err, result) => {
  let accounts = await Account.find({});
  console.log('accounts', accounts.length)
  accounts.forEach(async (account) => {
    /*
    let account_info = await blurt.api.getAccountsAsync([account.name]);
    console.log('account_info', account_info)
    */
    blurt.api.getAccounts([account.name], function (err, result) {
      console.log(err, result)
    });
  })

});