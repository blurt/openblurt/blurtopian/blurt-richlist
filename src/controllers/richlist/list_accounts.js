const _ = require('lodash');

const list_accounts = ({ Account }, { config }) => async (req, res, next) => {
  let { sort_by, sort_dir, limit } = req.query;
  limit = parseInt(limit || 100);

  let sort_keys = [
    'name', 'balance', 'vesting_shares', 'vesting_withdraw_rate', 'next_vesting_withdrawal', 'last_active_date',
    'delegated_vesting_shares', 'received_vesting_shares', 'savings_balance',
  ]
  let sort_dirs = ['1', '-1', 'asc', 'desc']

  sort_by = sort_by || 'vesting_shares';
  sort_dir = sort_dir || 'desc';
  let sort_obj = {}
  if (sort_keys.indexOf(sort_by) >= 0 && sort_dirs.indexOf(sort_dir) >= 0) {
    sort_obj[sort_by] = sort_dir;
  }

  try {
    let items = await Account.find({}).sort(sort_obj).limit(limit);
    res.status(200).send({ data: items });
  } catch (error) {
    console.log('error', error)
    next(error);
  }
}

module.exports = { list_accounts };