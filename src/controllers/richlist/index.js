const { Router: router } = require('express');

const { list_accounts } = require('./list_accounts');

module.exports = (models, { config }) => {
  const api = router();

  api.get('/', list_accounts(models, { config }));

  return api;
};
