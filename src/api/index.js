const express = require('express');

const { errorHandler } = require('../middleware');

// list of controllers here
const richlist = require('../controllers/richlist')

const routersInit = (config, models) => {
    const router = express();

    // register api points
    router.use('/richlist', richlist(models, { config }));

    // catch all api errors
    router.use(errorHandler);

    return router;
};

module.exports = routersInit;