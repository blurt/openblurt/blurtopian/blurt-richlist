const mongoose = require('mongoose');
const { Schema } = mongoose;

const LastProcessedSchema = new Schema({
  key: {
    type: String,
    required : true,
  },
  account: {
    type: String,
  },
});

module.exports = { LastProcessedSchema };