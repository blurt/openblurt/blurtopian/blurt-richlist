const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AccountSchema = new Schema({
  name: {
    type: String,
    required : true,
    unique: true,
  },
  balance: {
    type: Number,
  },
  vesting_shares: {
    type: Number,
  },
  vesting_withdraw_rate: {
    type: Number,
  },
  next_vesting_withdrawal: {
    type: Date,
  },
  delegated_vesting_shares: {
    type: Number,
  },
  received_vesting_shares: {
    type: Number,
  },
  savings_balance: {
    type: Number,
  },
  total_value: {
    type: Number,
  },
  last_active_date: {
    type: Date,
  },
});

AccountSchema.pre('save', next => {
  this.total_value = (this.vesting_shares + this.balance + this.savings_balance).toFixed(6);
  next();
});

module.exports = { AccountSchema };