const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AccountSchema = new Schema({
  name: {
    type: String,
    required : true,
    unique: true,
  },
  balance: {
    type: String,
  },
  vesting_shares: {
    type: String,
  },
  vesting_withdraw_rate: {
    type: String,
  },
  next_vesting_withdrawal: {
    type: String,
  },
});

module.exports = { AccountSchema };