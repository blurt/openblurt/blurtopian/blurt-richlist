const { AccountSchema } = require('./account');
const { LastProcessedSchema } = require('./last_processed');

module.exports = {
  AccountSchema,
  LastProcessedSchema,
};