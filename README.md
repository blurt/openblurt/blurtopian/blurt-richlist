## How to use it

Create a `.env` and place MongoDB URL to store accoutns.

```
MONGODB_URI=mongodb://localhost:27017/blurt_richlist
RPC_URL=https://rpc.blurt.world
```

Start the server for development: `npm run dev`.